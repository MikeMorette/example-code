## Описание

В данном репозитории находится пример кода REST приложения на Nest.JS.

- /api/ - Модули
- /api/users - Работа с пользователями
- /api/manage - Работа с системой
- /api/instagram - Работа с API Instagram
- /api/tiktok - Работа с API TikTok
