import { Body, Controller, Req, UsePipes } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { AuthUserDto } from '../../models/dto/auth-user.dto';
import { CreateUserDto } from '../../models/dto/create-user.dto';
import { User } from '../../models/users.model';
import { ValidationPipe } from '../../_core/pipes/validation.pipe';
import { XBPost, XGet } from '../../_core/decorators/decorator';
import { UsersService } from './users.service';

@ApiTags('Пользователи')
@Controller('users')
export class UsersController {

  constructor(
    protected readonly usersService: UsersService
  ) {}
  
  /**
   * Создать пользователя
   * @param dto CreateUserDto
   * @returns {token, user, state} | HttpException
   */
  @XBPost("Создать пользователя", "/account", User)
  @UsePipes(ValidationPipe)
  async createAccount(@Body() dto: CreateUserDto) {
    return await this.usersService.createAccount(dto);
  }

  /**
   * Данные пользователя
   * @param req Request
   * @returns {user}
   */
  @XGet("Данные пользователя", "/account", User)
  async getAccount(@Req() req: Request) {
    return await this.usersService.getAccount(req);
  }

  /**
   * Авторизация пользователя
   * @param dto AuthUserDto
   * @returns {token, user}
   */
  @XBPost("Авторизация пользователя", "/account/auth", User)
  @UsePipes(ValidationPipe)
  async signIn(@Body() dto: AuthUserDto) {
    return await this.usersService.signIn(dto);
  }

}
