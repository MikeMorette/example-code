import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { JwtModule } from '@nestjs/jwt';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from '../../models/users.model';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { ApiLogs } from '../../models/api-logs.model';

@Module({
  imports: [
    SequelizeModule.forFeature([User, ApiLogs]),
    HttpModule,
    JwtModule.register({
      secret: process.env.SECRET_KEY || 'secret',
      signOptions: {
        expiresIn: '24h'
      }
    }),
  ],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [UsersService, UsersModule, JwtModule]
})
export class UsersModule {
  constructor() {}
}
