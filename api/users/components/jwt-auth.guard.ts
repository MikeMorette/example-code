import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectModel } from "@nestjs/sequelize";
import { ApiLogs } from "../../../models/api-logs.model";
import { User } from "../../../models/users.model";
import { Logger } from "../../../_core/logger";

@Injectable()
export class JwtAuthGuard implements CanActivate {

  constructor(
    protected readonly jwtService: JwtService,
    @InjectModel(User) protected readonly userRepo: typeof User,
    @InjectModel(ApiLogs) protected readonly apiLogsRepo: typeof ApiLogs
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req: any = context.switchToHttp().getRequest();
    try {
      const authHeader = req.headers.authorization;
      const bearer = authHeader.split(" ")[0];
      const token = authHeader.split(" ")[1];

      if (bearer !== 'Bearer' || !token) {
        throw new UnauthorizedException({message: 'Пользователь не авторизован'});
      }

      if (token == "super_api_key") {
        req.user = {id: 2};
        Logger.log(`Semi-Authed Request -> ${req.url} -> ${req.user.email}`);
        const log = await this.apiLogsRepo.create({
          url: req.url,
          request: req.headers,
          data: req.body,
          params: req.params,
          authed: true,
          authedUser: 2
        });
        return true;
      }

      const user = this.jwtService.verify(token);
      const account = await this.userRepo.findOne({where: {id: user.id}});
      req.user = account["dataValues"];

      Logger.log(`Authed Request -> ${req.url} -> ${req.user.email}`);
      
      const log = await this.apiLogsRepo.create({
        url: req.url,
        request: req.headers,
        data: req.body,
        params: req.params,
        authed: true,
        authedUser: user.id
      });

      return true;

    } catch (e) {
      throw new UnauthorizedException({message: 'Пользователь не авторизован'});
    }
  }

}