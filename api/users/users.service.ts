import { HttpException, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/sequelize';
import * as bcrypt from 'bcryptjs';
import { AuthUserDto } from '../../models/dto/auth-user.dto';
import { CreateUserDto } from '../../models/dto/create-user.dto';
import { User } from '../../models/users.model';
import { isNullOrUndefined } from '../../_core/funcs';

@Injectable()
export class UsersService {

  constructor(
    @InjectModel(User) protected readonly userRepo: typeof User,
    protected readonly jwtService: JwtService
  ) {}

  public async createAccount(dto: CreateUserDto) {
    const candidate = await this.userRepo.findOne({where: {email: dto.email}});
    if (isNullOrUndefined(candidate)) {

      const hashPassword = await bcrypt.hash(dto.password, 5);

      const user = await this.userRepo.create({...dto, password: hashPassword});

      return {...this.generateToken(user), state: 1};
    } else {
      throw new HttpException("Пользователь с таким Email уже создан!", 400);
    }
  }

  public async getAccount(req: any) {
    const user = await this.userRepo.findOne({where: {id: req.user.id}});
    return {user};
  }

  public async signIn(dto: AuthUserDto) {
    const user = await this.validateUser({...dto});
    return this.generateToken(user);
  }

  private async generateToken(user: User) {
    const payload = {id: user.id, email: user.email};
    const token = this.jwtService.sign(payload, {});
    await this.userRepo.update({token}, {where: {id: user.id}});
    return { token, user };
  }

  private async validateUser(dto: AuthUserDto) {
    const user: User = await this.userRepo.findOne({where: {email: dto.email}});
    if (user) {
      const passwordEquals = await bcrypt.compare(dto.password, user.password);
      if (passwordEquals) {
        return user;
      } else {
        throw new UnauthorizedException({message: 'Некорректный email и/или пароль!'});
      }
    }
    throw new UnauthorizedException({message: 'Пользователь с таким email не найден!'});
  }

}
