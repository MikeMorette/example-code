import { Controller, Req, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';

import { User } from '../../models/users.model';

import { XGet } from '../../_core/decorators/decorator';

import { TikTokService } from './tiktok.service';

@ApiTags('Связь с API TikTok')
@Controller('tiktok')
export class TikTokController {

  constructor(
    protected readonly tiktokService: TikTokService
  ) {}
  
  @XGet("Данные пользователя", "/account", User)
  async getAccount(@Req() req: Request, @Res() res: Response) {
    return await this.tiktokService.getData(req, res);
  }

}
