import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from '../../models/users.model';
import { TTService } from '../../services/tiktok/tt.service';

@Injectable()
export class TikTokService {

  constructor(
    @InjectModel(User) protected readonly userRepo: typeof User,
    protected readonly ttService: TTService
  ) {}

  async getData(req: any, res: any) {
  
  }
}
