import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { SequelizeModule } from '@nestjs/sequelize';

import { User } from '../../models/users.model';
import { ApiLogs } from 'src/models/api-logs.model';

import { UsersModule } from '../users/users.module';
import { TikTokController } from './tiktok.controller';
import { TikTokService } from './tiktok.service';
import { TTService } from '../../services/tiktok/tt.service';

@Module({
  imports: [
    SequelizeModule.forFeature([User, ApiLogs]),
    HttpModule,
    UsersModule
  ],
  providers: [TikTokService, TTService],
  controllers: [TikTokController],
  exports: [TikTokService, TikTokModule]
})
export class TikTokModule {
  constructor() {}
}
