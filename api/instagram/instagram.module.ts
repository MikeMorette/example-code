import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from '../../models/users.model';
import { UsersModule } from '../users/users.module';
import { InstagramController } from './instagram.controller';
import { InstagramService } from './instagram.service';
import { IgService } from '../../services/instagram/ig.service';

import { BullModule } from '@nestjs/bull';
import { IgAccount } from '../../models/ig-accounts.model';
import { Proxy } from '../../models/proxies.model';
import { Log } from '../../models/logs.model';
import { IgManager } from '../../services/instagram/ig.manager';
import { IgWAPIService } from '../../services/instagram/ig-wapi.service';
import { InstagramOpenController } from './ig-open.controller';
import { ApiLogs } from '../../models/api-logs.model';
import { Settings } from 'src/models/settings.model';
import { Categories } from 'src/models/categories.model';
import { IgParserService } from 'src/services/instagram/ig-parser.service';
import { IgServices } from 'src/models/ig-services.model';

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'instagram'
    }),
    SequelizeModule.forFeature([User, IgAccount, Proxy, Log, ApiLogs, Settings, Categories, IgServices]),
    HttpModule,
    UsersModule,
  ],
  providers: [InstagramService, IgService, IgWAPIService, IgManager, IgParserService],
  controllers: [InstagramController, InstagramOpenController],
  exports: [InstagramService, InstagramModule]
})
export class InstagramModule {
  constructor() {}
}
