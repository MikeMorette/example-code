import { Controller, Param, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { IgMethods } from '../../services/instagram/ig.constants';
import { User } from '../../models/users.model';
import { XBGet } from '../../_core/decorators/decorator';
import { InstagramService } from './instagram.service';

/**
 * @class InstagramOpenController
 * @description Связь с API Instagram. Открытый доступ.
 */
@ApiTags('Связь с API Instagram. Открытый доступ.')
@Controller('instagram/open')
export class InstagramOpenController {

  constructor(
    protected readonly instagramService: InstagramService
  ) {}
  
  /**
   * Основная информация об аккаунте
   * @name getAccountInfo
   * @param req - Тело запроса
   * @param aid - Идентификатор IG аккаунта
   * @returns Object - Ответ от IG
   */
  @XBGet("Основная информация об аккаунте", "/account/info/:aid", User)
  async getAccountInfo(@Req() req: any, @Param('aid') aid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.ACCOUNT_INFO, aid);
  }
  
  /**
   * Счетчики публикаций/подписчиков/подписок
   * @name getAccountCounters
   * @param req - Тело запроса
   * @param aid - Идентификатор IG аккаунта
   * @returns Object - Ответ от IG
   */
  @XBGet("Счетчики публикаций/подписчиков/подписок", "/account/counters/:aid", User)
  async getAccountCounters(@Req() req: any, @Param('aid') aid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.ACCOUNT_COUNTERS, aid);
  }
  
  /**
   * Публикации аккаунта
   * @name getAccountMedia
   * @param req - Тело запроса
   * @param aid - Идентификатор IG аккаунта
   * @returns Object - Ответ от IG
   */
  @XBGet("Публикации аккаунта", "/account/media/:aid", User)
  async getAccountMedia(@Req() req: any, @Param('aid') aid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.ACCOUNT_MEDIA, aid);
  }
  
  /**
   * Список подписчиков аккаунта
   * @name getAccountSubs
   * @param req - Тело запроса
   * @param aid - Идентификатор IG аккаунта
   * @returns Object - Ответ от IG
   */
  @XBGet("Список подписчиков аккаунта", "/account/subs/:aid", User)
  async getAccountSubs(@Req() req: any, @Param('aid') aid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.ACCOUNT_SUBS, aid);
  }
  
  /**
   * Список подписок аккаунта
   * @name getAccountMySubs
   * @param req - Тело запроса
   * @param aid - Идентификатор IG аккаунта
   * @returns Object - Ответ от IG
   */
  @XBGet("Список подписок аккаунта", "/account/mysubs/:aid", User)
  async getAccountMySubs(@Req() req: any, @Param('aid') aid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.ACCOUNT_MY_SUBS, aid);
  }
  
  /**
   * Сторисы аккаунта с данными о длительности
   * @name getAccountStories
   * @param req - Тело запроса
   * @param aid - Идентификатор IG аккаунта
   * @returns Object - Ответ от IG
   */
  @XBGet("Сторисы аккаунта с данными о длительности", "/account/stories/:aid", User)
  async getAccountStories(@Req() req: any, @Param('aid') aid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.ACCOUNT_STORIES, aid);
  }
  
  /**
   * Основная информация о публикации
   * @name getMediaInfo
   * @param req - Тело запроса
   * @param mid - Идентификатор публикации
   * @returns Object - Ответ от IG
   */
  @XBGet("Основная информация о публикации", "/media/info/:mid", User)
  async getMediaInfo(@Req() req: any, @Param('mid') mid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.MEDIA_INFO, mid);
  }
  
  /**
   * Список лайков публикации
   * @name getMediaLikes
   * @param req - Тело запроса
   * @param mid - Идентификатор публикации
   * @returns Object - Ответ от IG
   */
  @XBGet("Список лайков публикации", "/media/likes/:mid", User)
  async getMediaLikes(@Req() req: any, @Param('mid') mid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.MEDIA_LIKES, mid);
  }
  
  /**
   * Список комментов публикации
   * @name getMediaComments
   * @param req - Тело запроса
   * @param mid - Идентификатор публикации
   * @returns Object - Ответ от IG
   */
  @XBGet("Список комментов публикации", "/media/comments/:mid", User)
  async getMediaComments(@Req() req: any, @Param('mid') mid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.MEDIA_COMMENTS, mid);
  }
  
  /**
   * Список лайкеров коммента публикации
   * @name getMediaCommentLikers
   * @param req - Тело запроса
   * @param mid - Идентификатор публикации
   * @param url? - Ссылка на публикацию (Вместо использования прямого mid)
   * @param username! - ID владельца комментария
   * @returns Object - Ответ от IG
   */
  @XBGet("Список лайкеров коммента публикации", "/media/comment/likers/:mid", User)
  async getMediaCommentLikers(@Req() req: any, @Param('mid') mid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.MEDIA_COMMENT_LIKERS, mid);
  }

  /**
   * Счетчики лайков/комментов
   * @name getMediaCounters
   * @param req - Тело запроса
   * @param mid - Идентификатор публикации
   * @returns Object - Ответ от IG
   */
  @XBGet("Счетчики лайков/комментов", "/media/counters/:mid", User)
  async getMediaCounters(@Req() req: any, @Param('mid') mid: string) {
    req.user = {id: 2};
    return await this.instagramService.getData(req, IgMethods.MEDIA_COUNTERS, mid);
  }

}
