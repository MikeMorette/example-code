import { BeforeApplicationShutdown, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { IgAccount } from '../../models/ig-accounts.model';
import { Proxy } from '../../models/proxies.model';
import { User } from '../../models/users.model';
import { InstagramImplFirst } from '../../_core/types/ig.type';
import { Log } from '../../models/logs.model';
import { IgMethodName, IgMethods } from '../../services/instagram/ig.constants';
import { IgManager } from '../../services/instagram/ig.manager';
import { Logger } from '../../_core/logger';
import { Op } from 'sequelize';
import { isNullOrUndefined } from 'src/_core/funcs';
import { Settings } from 'src/models/settings.model';
import { IgServices } from 'src/models/ig-services.model';

@Injectable()
export class InstagramService implements OnApplicationBootstrap, InstagramImplFirst {

  constructor(
    @InjectModel(User) protected readonly userRepo: typeof User,
    @InjectModel(IgAccount) protected readonly igAccountRepo: typeof IgAccount,
    @InjectModel(Log) protected readonly logsRepo: typeof Log,
    @InjectModel(Proxy) protected readonly proxyRepo: typeof Proxy,
    @InjectModel(Settings) protected readonly settingsRepo: typeof Settings,
    @InjectModel(IgServices) protected readonly igServicesRepo: typeof IgServices,
    protected readonly igManager: IgManager
  ) {
  }

  public async onApplicationBootstrap() {
    Logger.slog(`[onApplicationBootstrap] Unload all IG services.`);
    
    await this.igAccountRepo.update({status: 1}, {where: {status: {[Op.not]: 3}}});
    await this.proxyRepo.update({status: 1}, {where: {status: {[Op.not]: 3}}});
    await this.igServicesRepo.destroy({where: {}});

    this.igManager.init();
  }

  public async getData(req: any, method: IgMethodName, aid: any) {
    if (aid == "0" && req.query.url) {
      aid = this.parseInstagramAid(req.query.url, method);
    }

    // Method status controll
    const methodsStates = await this.settingsRepo.findOne({where: {name: 'methods'}});
    const thisMethod = methodsStates.value.value.find(v => v.name == method);
    if (!isNullOrUndefined(thisMethod) && thisMethod.status == 0) {
      return {error: true, message: "Данный метод не активен! Включите его в Панели Управления (https://cp.igutils.ru/main/instagram)"};
    }

    let data = null;
    if (aid != null) {
      data = await this.igManager.action(req, method, aid);
    }
    return data;
  }

  private parseInstagramAid(url: string, method: IgMethodName) {
    let index = 0;

    if (method === IgMethods.MEDIA_INFO || method === IgMethods.MEDIA_COUNTERS
      || method === IgMethods.MEDIA_COMMENTS || method === IgMethods.MEDIA_LIKES) {
      index = 4;
    } else {
      index = 3;
    }

    if (url.includes("instagram.com")) {
      if (url.includes("https://") || url.includes("http://")) {
        return url.split("/")[index];
      } else {
        return url.split("/")[index - 2];
      }
    } else {
      return null;
    }
  }

}
