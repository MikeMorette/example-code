import { Body, Controller, Param, ParseBoolPipe, ParseIntPipe, Post, Query, Req, UploadedFile, UseInterceptors, UsePipes } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { IgAccount } from '../../models/ig-accounts.model';
import { Log } from '../../models/logs.model';
import { Proxy } from '../../models/proxies.model';

import { CreateIgAccountDto } from '../../models/dto/create-igacc.dto';
import { CreateLogDto } from '../../models/dto/create-log.dto';
import { CreateProxyDto } from '../../models/dto/create-proxy.dto';

import { PaginationValidationPipe } from '../../_core/pipes/pagination.pipe';
import { ValidationPipe } from '../../_core/pipes/validation.pipe';
import { XBGet, XDelete, XGet, XPost, XPut } from '../../_core/decorators/decorator';

import { ManageService } from './manage.service';
import { Request } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { Settings } from 'src/models/settings.model';
import { CreateSettingDto } from 'src/models/dto/create-setting.dto';
import { CreateCatDto } from 'src/models/dto/create-cat.dto';

class StatsModel {

}

/**
 * @class ManageController
 * @description Управление системой
 */
@ApiTags('Управление системой')
@Controller('manage')
export class ManageController {

  constructor(
    protected readonly manageService: ManageService
  ) {}

  /**
   * Загрузить аккаунты/прокси
   */
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(@Req() req: Request, @UploadedFile() file: Express.Multer.File) {
    const data: string = file.buffer.toString();
    return {data};
  }

  /**
   * Категории
   * @returns `Categories[]`
   */
   @XGet("Получить категории", "/cats", {})
   async getCats(
     @Req() req: Request
   ) {
     return await this.manageService.getCats(req);
   }
 
   /**
    * Создать категорию
    * @param dto CreateSettingDto
    * @returns `Categories[]`
    */
   @XPost("Создать категорию", "/cats", {})
   async createCats(@Body() dto: CreateCatDto) {
     return await this.manageService.createCats(dto);
   }
 
   /**
    * Добавить аккаунты в категорию
    * @returns `Categories[]`
    */
  @XPost("Добавить аккаунты в категорию", "/cats/:id/addaccs", {})
  async addAccsToCat(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: any
  ) {
    return await this.manageService.addAccsToCat(id, body);
  }
 
   /**
    * Обновить категорию
    * @param id number
    * @param dto CreateSettingDto
    * @returns `Categories[]`
    */
   @XPut("Обновить категорию", "/cats/:id", {})
   async updateCat(
     @Param('id', ParseIntPipe) id: number,
     @Body() dto: CreateCatDto
   ) {
     return await this.manageService.updateCat(id, dto);
   }
 
   /**
    * Удалить категорию
    * @param id number
    * @returns `Categories[]`
    */
   @XDelete("Удалить категорию", "/cats/:id", {})
   async deleteCat(@Param('id', ParseIntPipe) id: number) {
     return await this.manageService.deleteCat(id);
   }

  /**
   * Настройки
   * @returns `Settings[]`
   */
  @XGet("Получить настройки", "/settings", {})
  async getSettings(
    @Req() req: Request
  ) {
    return await this.manageService.getSettings(req);
  }

  /**
   * Создать настройки
   * @param dto CreateSettingDto
   * @returns `Settings[]`
   */
  @XPost("Создать настройку", "/settings", {})
  async createSetting(@Body() dto: CreateSettingDto) {
    return await this.manageService.createSetting(dto);
  }

  /**
   * Обновить настройки
   * @param id number
   * @param dto CreateSettingDto
   * @returns `Settings[]`
   */
  @XPut("Обновить настройку", "/settings/:id", {})
  async updateSetting(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: CreateSettingDto
  ) {
    return await this.manageService.updateSetting(id, dto);
  }

  /**
   * Удалить настройки
   * @param id number
   * @returns `Settings[]`
   */
  @XDelete("Удалить настройку", "/settings/:id", {})
  async deleteSetting(@Param('id', ParseIntPipe) id: number) {
    return await this.manageService.deleteSetting(id);
  }
 
  /**
   * Статистика
   * @param sort boolean
   * @param limit number
   * @param page number
   * @param query string
   * @returns `{list: Proxy[], pages: number, total: number}`
   */
  @XGet("Список прокси", "/stats", StatsModel)
  async getStats(
    @Req() req: Request
  ) {
    return await this.manageService.getStats(req);
  }

  /**
   * Список прокси
   * @param sort boolean
   * @param limit number
   * @param page number
   * @param query string
   * @returns `{list: Proxy[], pages: number, total: number}`
   */
  @XGet("Список прокси", "/proxies", Proxy)
  @UsePipes(PaginationValidationPipe)
  async getProxyList(
    @Query('sort', ParseBoolPipe) sort: boolean,
    @Query('limit', ParseIntPipe) limit: number,
    @Query('page', ParseIntPipe) page: number,
    @Query('query') query: string,
  ) {
    return await this.manageService.getProxyList(sort, limit, page, query);
  }

  /**
   * Добавить прокси
   * @param dto CreateProxyDto
   * @returns `{message: string, result: any}`
   */
  @XPost("Добавить прокси", "/proxies", Proxy)
  @UsePipes(ValidationPipe)
  async addProxy(@Body() dto: CreateProxyDto) {
    return await this.manageService.addProxy(dto);
  }

  /**
   * Обновить прокси
   * @param id number
   * @param dto CreateProxyDto
   * @returns `{message: string, result: any}`
   */
  @XPut("Обновить прокси", "/proxies/:id", Proxy)
  @UsePipes(ValidationPipe)
  async updateProxy(
    @Param('id', ParseIntPipe) id: number, 
    @Body() dto: CreateProxyDto
  ) {
    return await this.manageService.updateProxy(id, dto);
  }

  /**
   * Удалить прокси
   * @param id number
   * @returns `{message: string, result: any}`
   */
  @XDelete("Удалить прокси", "/proxies/:id", Proxy)
  @UsePipes(ValidationPipe)
  async deleteProxy(@Param('id', ParseIntPipe) id: number) {
    return await this.manageService.deleteProxy(id);
  }

  /**
   * Удалить все IG Аккаунты
   * @param id number
   * @returns `{message: string, result: any}`
   */
  @XDelete("Удалить все лог записи", "/proxies", Proxy)
  @UsePipes(ValidationPipe)
  async deleteAllProxy() {
    return await this.manageService.deleteAllProxy();
  }

  /**
   * Список IG Аккаунтов
   * @param sort boolean
   * @param limit number
   * @param page number
   * @param query string
   * @returns `{list: Proxy[], pages: number, total: number}`
   */
  @XGet("Список IG Аккаунтов", "/igaccounts", IgAccount)
  @UsePipes(PaginationValidationPipe)
  async getIGAccountsList(
    @Query('sort', ParseBoolPipe) sort: boolean,
    @Query('limit', ParseIntPipe) limit: number,
    @Query('page', ParseIntPipe) page: number,
    @Query('query') query: string,
    @Query('filter') filter: string
  ) {
    return await this.manageService.getIGAccountsList(sort, limit, page, query, filter);
  }

  @XGet("Список всех IG Аккаунтов", "/alligaccounts", IgAccount)
  async getAllIGAccountsList() {
    return await this.manageService.getAllIGAccounts();
  }

  /**
   * Добавить IG Аккаунт
   * @param dto CreateIgAccountDto
   * @returns `{message: string, result: any}`
   */
  @XPost("Добавить IG Аккаунт", "/igaccounts", IgAccount)
  @UsePipes(ValidationPipe)
  async addIGAccount(@Body() dto: CreateIgAccountDto) {
    return await this.manageService.addIGAccount(dto);
  }

  /**
   * Добавить IG Аккаунты
   * @param dto CreateIgAccountDto
   * @returns `{message: string, result: any}`
   */
  @XPost("Добавить IG Аккаунты", "/igaccounts/many", IgAccount)
  @UsePipes(ValidationPipe)
  async addIGAccounts(@Req() req: any) {
    return await this.manageService.addIGAccounts(req);
  }

  /**
   * Обновить IG Аккаунт
   * @param id number
   * @param dto 
   * @returns `{message: string, result: any}`
   */
  @XPut("Обновить IG Аккаунт", "/igaccounts/:id", IgAccount)
  @UsePipes(ValidationPipe)
  async updateIGAccount(@Param('id', ParseIntPipe) id: number, @Body() dto: CreateIgAccountDto) {
    return await this.manageService.updateIGAccount(id, dto);
  }

  /**
   * Удалить IG Аккаунт
   * @param id number
   * @returns `{message: string, result: any}`
   */
  @XDelete("Удалить IG Аккаунт", "/igaccounts/:id", IgAccount)
  @UsePipes(ValidationPipe)
  async deleteIGAccount(@Param('id', ParseIntPipe) id: number) {
    return await this.manageService.deleteIGAccount(id);
  }

  /**
   * Удалить все IG Аккаунты
   * @param id number
   * @returns `{message: string, result: any}`
   */
  @XDelete("Удалить все лог записи", "/igaccounts", IgAccount)
  @UsePipes(ValidationPipe)
  async deleteIGAccs() {
    return await this.manageService.deleteIGAccs();
  }

  /**
   * Получить отдельный лог
   * @param id number - Идентификатор лога
   * @returns `{log: Log}`
   */
  @XBGet("Получить отдельный лог", "/logs/:id", {})
  async getLog(
    @Param('id', ParseIntPipe) id: number
  ) {
    return await this.manageService.getLog(id);
  }

  /**
   * Логи системы
   * @param sort boolean
   * @param limit number
   * @param page number
   * @param query string
   * @returns `{list: Proxy[], pages: number, total: number}`
   */
  @XGet("Логи системы", "/logs", Log)
  @UsePipes(PaginationValidationPipe)
  async getLogs(
    @Query('sort', ParseBoolPipe) sort: boolean,
    @Query('limit', ParseIntPipe) limit: number,
    @Query('page', ParseIntPipe) page: number,
    @Query('query') query: string,
    @Query('filter') filter: string
  ) {
    return await this.manageService.getLogs(sort, limit, page, query, filter);
  }

  /**
   * Создать лог запись
   * @param dto CreateLogDto
   * @returns `{message: string, result: any}`
   */
  @XPost("Создать лог запись", "/logs", Log)
  @UsePipes(ValidationPipe)
  async createLog(@Body() dto: CreateLogDto) {
    return await this.manageService.createLog(dto);
  }

  /**
   * Обновить лог запись
   * @param id number
   * @param dto CreateLogDto
   * @returns `{message: string, result: any}`
   */
  @XPut("Обновить лог запись", "/logs/:id", Log)
  @UsePipes(ValidationPipe)
  async updateLog(@Param('id', ParseIntPipe) id: number, @Body() dto: CreateLogDto) {
    return await this.manageService.updateLog(id, dto);
  }

  /**
   * Удалить лог запись
   * @param id number
   * @returns `{message: string, result: any}`
   */
  @XDelete("Удалить лог запись", "/logs/:id", Log)
  @UsePipes(ValidationPipe)
  async deleteLog(@Param('id', ParseIntPipe) id: number) {
    return await this.manageService.deleteLog(id);
  }

  /**
   * Удалить все лог записи
   * @param id number
   * @returns `{message: string, result: any}`
   */
  @XDelete("Удалить все лог записи", "/logs", Log)
  @UsePipes(ValidationPipe)
  async deleteLogs() {
    return await this.manageService.deleteLogs();
  }

  /**
   * Перезагрузить сервис
   * @param serviceName string Наименование сервиса
   * @returns `{message: string, result: any}`
   */
  @XGet("Перезагрузить сервис", "/restart/:serviceName", {})
  async restartService(
    @Param('serviceName') serviceName: string
  ) {
    return await this.manageService.restartService(serviceName);
  }

}
