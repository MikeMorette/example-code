import {
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer
} from '@nestjs/websockets';
import { Logger } from 'src/_core/logger';
import { Socket, RemoteSocket, Server } from 'socket.io';
import { Cron } from '@nestjs/schedule';
import { Log } from 'src/models/logs.model';
import { Proxy } from 'src/models/proxies.model';
import { IgAccount } from 'src/models/ig-accounts.model';
import { ManageService } from './manage.service';
import { InjectModel } from '@nestjs/sequelize';
import { asyncForEach, isNullOrUndefined } from 'src/_core/funcs';

@WebSocketGateway({
  cors: {
    origin: ["https://cp.socapi.ru", "http://localhost:4200"],
    credentials: true
  }
})
export class ManageGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  
  @WebSocketServer() server: Server;

  constructor(
    @InjectModel(Log) protected readonly logsRepo: typeof Log,
    @InjectModel(IgAccount) protected readonly igAccsRepo: typeof IgAccount,
    @InjectModel(Proxy) protected readonly proxyRepo: typeof Proxy,
    protected readonly manageService: ManageService
  ) {}

  public afterInit(server: Server) {
    Logger.slog('Init socket gateway!');
  }

  public handleDisconnect(client: Socket) {
    Logger.slog(`Client disconnected: ${client.id}`);
  }

  public handleConnection(client: Socket, ...args: any[]) {
    Logger.slog(`Client connected: ${client.id}`);
  }

  @Cron('*/5 * * * * *')
  public async globalEmitStats() {
    const socks = await this.server.fetchSockets();

    if (socks.length > 0) {
      await asyncForEach(socks, async (v: RemoteSocket<any>, i: number, a: Socket[]) => {
        const period = isNullOrUndefined(v.data.period) ? 'day' : v.data.period;
        const stats = await this.manageService.getStats({query: {period}});
        const totalLogs = await this.logsRepo.count({});
        v.emit('stats:get', {stats});
        v.emit('logs:get', {totalLogs});
      });
    }
  }

  @SubscribeMessage('stats:filter')
  public async filter(client: Socket, data: any): Promise<any> {
    client.data.period = data.period;
    return true;
  }

}