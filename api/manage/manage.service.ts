import { BeforeApplicationShutdown, OnApplicationBootstrap, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Op } from 'sequelize';
import { Logger } from 'src/_core/logger';
import { CreateIgAccountDto } from '../../models/dto/create-igacc.dto';
import { CreateLogDto } from '../../models/dto/create-log.dto';
import { CreateProxyDto } from '../../models/dto/create-proxy.dto';
import { IgAccount } from '../../models/ig-accounts.model';
import { Log } from '../../models/logs.model';
import { Proxy } from '../../models/proxies.model';
import { User } from '../../models/users.model';

import * as childProcess from 'child_process';
import { asyncForEach, isNullOrUndefined } from 'src/_core/funcs';
import { CreateSettingDto } from 'src/models/dto/create-setting.dto';
import { Settings } from 'src/models/settings.model';
import { CreateCatDto } from 'src/models/dto/create-cat.dto';
import { Categories } from 'src/models/categories.model';
import * as moment from 'moment';
import { IgAccountModel } from 'src/_core/types';

@Injectable()
export class ManageService {

  constructor(
    @InjectModel(User) protected readonly userRepo: typeof User,
    @InjectModel(Proxy) protected readonly proxyRepo: typeof Proxy,
    @InjectModel(IgAccount) protected readonly igAccountRepo: typeof IgAccount,
    @InjectModel(Log) protected readonly logRepo: typeof Log,
    @InjectModel(Settings) protected readonly settingsRepo: typeof Settings,
    @InjectModel(Categories) protected readonly catsRepo: typeof Categories
  ) {}

  public async getCats(req: any) {
    const list = await this.catsRepo.findAll({
      order: [["id", "ASC"]]
    });
    return {total: list.length, list};
  }

  public async addAccsToCat(id: number, body: any) {
    let arr = [];
    for (let i = 0; i < body.count; i++) {
      arr.push({upd: null});
    }

    await asyncForEach(arr, async (v: any, i: number, a: any[]) => {
      const acc = await this.igAccountRepo.findOne({where: {categoryId: 1}});
      if (!isNullOrUndefined(acc)) {
        v.upd = await acc.update({categoryId: id});
      }
    });

    return {upd: arr};
  }

  public async createCats(dto: CreateCatDto) {
    const created = await this.catsRepo.create({...dto});
    return {created};
  }

  public async updateCat(id: number, dto: CreateCatDto) {
    const updated = await this.catsRepo.update({...dto}, {
      where: {id}
    });
    return {updated};
  }

  public async deleteCat(id: number) {
    const rem = await this.catsRepo.destroy({where: {id}});
    return {rem};
  }

  public async getSettings(req: any) {
    const list = await this.settingsRepo.findAll({});
    return {total: list.length, list};
  }

  public async createSetting(dto: CreateSettingDto) {
    const created = await this.settingsRepo.create({...dto});
    return {created};
  }

  public async updateSetting(id: number, dto: CreateSettingDto) {
    const exists = await this.settingsRepo.findOne({where: {id}});
    if (!isNullOrUndefined(exists)) {
      if (exists.name == 'logs_res_saving' && dto.value == '0') {
        await this.logRepo.update({ response: null }, {where: {}});
      }

      const updated = await this.settingsRepo.update({...dto}, {
        where: {id}
      });
      return {updated};
    } else {
      return {updated: null};
    }
  }

  public async deleteSetting(id: number) {
    const rem = await this.settingsRepo.destroy({where: {id}});
    return {rem};
  }

  public async getStats(req: any) {
    const counters = {
      logs: {
        success: 0, error: 0, queued: 0,
        chart: [[0,0,0,0,0,0,0], [0,0,0,0,0,0,0]]
      },
      igaccs: {
        deactive: 0, work: 0, active: 0, killed: 0
      },
      proxy: {
        deactive: 0, work: 0, active: 0, killed: 0
      },
      methods: {

      }
    };

    let findPeriod: any = {};
    console.log(req.query.period);
    if (!isNullOrUndefined(req.query.period)) {
      if (req.query.period == 'day') {
        findPeriod.createdAt = {[Op.between]: [(moment().startOf('day').unix() * 1000), (moment().endOf('day').unix() * 1000)]};
      } else if (req.query.period == 'week') {
        findPeriod.createdAt = {[Op.between]: [(moment().add('-1', 'day').startOf('week').unix() * 1000), (moment().add('-1', 'day').endOf('week').unix() * 1000)]};
      } else if (req.query.period == 'month') {
        findPeriod.createdAt = {[Op.between]: [(moment().startOf('month').add('+1', 'day').unix() * 1000), (moment().endOf('month').add('+1', 'day').unix() * 1000)]};
      } else if (req.query.period == 'year') {
        findPeriod.createdAt = {[Op.between]: [(moment().startOf('year').unix() * 1000), (moment().endOf('year').unix() * 1000)]};
      }
    }

    counters.logs.success = await this.logRepo.count({where: {...findPeriod, [Op.or]: [{status: 0}, {status: 1}]}});
    counters.logs.error = await this.logRepo.count({where: {...findPeriod, status: 2}});
    counters.logs.queued = await this.logRepo.count({where: {...findPeriod, status: 3}});

    counters.igaccs.deactive = await this.igAccountRepo.count({where: {status: 0}});
    counters.igaccs.active = await this.igAccountRepo.count({where: {status: 1}});
    counters.igaccs.work = await this.igAccountRepo.count({where: {status: 2}});
    counters.igaccs.killed = await this.igAccountRepo.count({where: {status: 3}});

    counters.proxy.deactive = await this.proxyRepo.count({where: {status: 0}});
    counters.proxy.active = await this.proxyRepo.count({where: {status: 1}});
    counters.proxy.work = await this.proxyRepo.count({where: {status: 2}});
    counters.proxy.killed = await this.proxyRepo.count({where: {status: 3}});
  
    const methods = [
      "getAccountInfo", "getAccountCounters",
      "getAccountMedia", "getAccountSubs",
      "getAccountMySubs", "getAccountStories",
      "getMediaInfo", "getMediaLikes",
      "getMediaComments", "getMediaCommentLikers", "getMediaCounters",
      "paused_request"
    ];

    await asyncForEach(methods, async (v: string, i: number, a: string[]) => {
      counters.methods[v] = {
        success: await this.logRepo.count({where: {request: {action: v}, status: 1}}),
        errors: await this.logRepo.count({where: {request: {action: v}, status: 2}})
      };
    });

    return {counters};
  }

  /**
   * Получить список прокси
   * @param sort boolean Указатель сортировки по дате (true=DESC/false=ASC)
   * @param limit number Кол-во записей на страницу
   * @param page number Страница
   * @param query string Текстовый поиск
   * @returns {list, pages, total}
   */
  public async getProxyList(sort: boolean, limit: number, page: number, query: string): Promise<{list: any[], pages: string, total: number}> {

    const list: any[] = await this.proxyRepo.findAll({
      where: {},
      order: [["createdAt", sort ? "DESC" : "ASC"]],
      limit: limit,
      offset: limit * page
    });

    const total = await this.proxyRepo.count({});

    return {
      list, pages: (total / limit).toFixed(0), total
    };

  }

  /**
   * Добавить прокси
   * @param dto CreateProxyDto Данные для добавления прокси
   * @returns {message, result}
   */
  public async addProxy(dto: CreateProxyDto): Promise<any> {
    const proxy = await this.proxyRepo.create({...dto});
    return {message: 'Прокси успешно добавлено', result: proxy};
  }

  /**
   * Обновить прокси
   * @param id number ID Прокси
   * @param dto CreateProxyDto Данные для обновления прокси
   * @returns {message, result}
   */
  public async updateProxy(id: number, dto: CreateProxyDto): Promise<any> {
    const upd = await this.proxyRepo.update({...dto}, {
      where: {id}
    });
    return {message: 'Прокси успешно обновлено', result: upd};
  }

  /**
   * Удалить прокси
   * @param id number ID Прокси
   * @returns {message, result}
   */
  public async deleteProxy(id: number): Promise<any> {
    const del = await this.proxyRepo.destroy({where: {id}});
    return {message: 'Прокси успешно удалено', result: del};
  }

  /**
   * Удалить все прокси
   * @returns {message, result}
   */
   public async deleteAllProxy(): Promise<any> {
    const del = await this.proxyRepo.destroy({where: {}});
    return {message: 'Прокси успешно удалены', result: del};
  }

  /**
   * Получить список IG аккаунтов
   * @param sort boolean Указатель сортировки по дате (true=DESC/false=ASC)
   * @param limit number Кол-во записей на страницу
   * @param page number Страница
   * @param query string Текстовый поиск
   * @returns {list, pages, total}
   */
  public async getIGAccountsList(sort: boolean, limit: number, page: number, query: string, filter: string): Promise<any> {

    let props: any = {};
    props.login = {[Op.like]: `${query}%`};

    if (filter == 'active') {
      props.status = 1;
    } else if (filter == 'killed') {
      props.status = 3;
    }

    const list = await this.igAccountRepo.findAll({
      where: props,
      order: [["createdAt", sort ? "DESC" : "ASC"]],
      limit: limit,
      offset: limit * page
    });

    const total = await this.igAccountRepo.count({ where: props });

    return {
      list, pages: (total / limit).toFixed(0), total
    };

  }

  public async getAllIGAccounts() {
    const list = await this.igAccountRepo.findAll({});
    return {list, total: list.length};
  }

  /**
   * Добавить IG аккаунт
   * @param dto CreateIgAccountDto Данные для добавления IG аккаунта
   * @returns {message, result}
   */
  public async addIGAccount(dto: CreateIgAccountDto): Promise<any> {

    const exists = await this.igAccountRepo.findOne({where: {
      login: dto.login,
      password: dto.password
    }});

    if (isNullOrUndefined(exists)) {
      const igAcc = await this.igAccountRepo.create({...dto});
      return {message: 'IG аккаунт успешно добавлен', result: igAcc};
    } else {
      return {message: 'Аккаунт с таким логином/паролем уже существует!', result: null};
    }

  }

  /**
   * Добавить IG аккаунты
   * @returns {cr: IgAccount[]}
   */
  public async addIGAccounts(req: any) {
    if (isNullOrUndefined(req.body.accs)) {
      return {error: true, message: "Укажите список аккаунтов!"};
    }

    const list: IgAccountModel[] = req.body.accs;
    let cr: any[] = [];

    await asyncForEach(list, async (acc: IgAccountModel, i: number, a: IgAccountModel[]) => {

      const exists = await this.igAccountRepo.findOne({where: {
        login: acc.login,
        password: acc.password
      }});

      if (isNullOrUndefined(exists)) {
        const item = await this.igAccountRepo.create({
          login: acc.login,
          password: acc.password
        });
        cr.push(item);
      }
    });

    return {cr};
  }

  /**
   * Обновить IG аккаунт
   * @param id number ID IG аккаунта
   * @param dto CreateIgAccountDto Данные для обновления IG аккаунта
   * @returns {message, result}
   */
  public async updateIGAccount(id: number, dto: CreateIgAccountDto): Promise<any> {
    const upd = await this.igAccountRepo.update({...dto}, {
      where: {id}
    });
    return {message: 'IG аккаунт успешно обновлен', result: upd};
  }

  /**
   * Удалить IG аккаунт
   * @param id number ID IG аккаунта
   * @returns {message, result}
   */
  public async deleteIGAccount(id: number): Promise<any> {
    const del = await this.igAccountRepo.destroy({where: {id}});
    return {message: 'IG аккаунт успешно удален', result: del};
  }

  /**
   * Удалить все IG аккаунты
   * @returns {message, result}
   */
  public async deleteIGAccs(): Promise<any> {
    const del = await this.igAccountRepo.destroy({where: {}});
    return {message: 'IG аккаунты успешно удалены', result: del};
  }

  /**
   * Получить ответ лога
   * @param id number ID лога
   * @returns {response}
   */
  public async getLog(id: number): Promise<any> {
    const log = await this.logRepo.findOne({where: {id}});
    let res = null;
    if (!isNullOrUndefined(log)) {
      res = log.response;
    }
    return {response: res};
  }

  /**
   * Получить список логов
   * @param sort boolean Указатель сортировки по дате (true=DESC/false=ASC)
   * @param limit number Кол-во записей на страницу
   * @param page number Страница
   * @param query string Текстовый поиск
   * @returns {list, pages, total}
   */
  public async getLogs(sort: boolean, limit: number, page: number, query: string, filter: string): Promise<any> {

    let props: any = {};
    if (query != "") {
      if (isNaN(query as any)) {

      } else {
        props.id = query;
      }
    }

    if (filter == 'success') {
      props.status = 1;
    } else if (filter == 'error') {
      props.status = 2;
    }

    const list = await this.logRepo.findAll({
      where: props,
      attributes: {exclude: ['response']},
      order: [["createdAt", sort ? "DESC" : "ASC"]],
      limit: limit,
      offset: limit * page
    });

    const total = await this.logRepo.count({ where: props });

    return {
      list, pages: (total / limit).toFixed(0), total
    };
  }

  /**
   * Создать лог
   * @param dto CreateLogDto Данные для создания лога
   * @returns {message, result}
   */
  public async createLog(dto: CreateLogDto): Promise<any> {
    const log = await this.logRepo.create({...dto});
    return {message: 'Лог успешно создан', result: log};
  }

  /**
   * Обновить лог
   * @param id number ID лога
   * @param dto CreateLogDto Данные для обновления лога
   * @returns {message, result}
   */
  public async updateLog(id: number, dto: CreateLogDto): Promise<any> {
    const upd = await this.logRepo.update({...dto}, {
      where: {id}
    });
    return {message: 'Лог успешно обновлен', result: upd};
  }

  /**
   * Удалить лог
   * @param id number ID лога
   * @returns {message, result}
   */
  public async deleteLog(id: number): Promise<any> {
    const del = await this.logRepo.destroy({where: {id}});
    return {message: 'Лог успешно удален', result: del};
  }

  /**
   * Удалить все логи
   * @returns {message, result}
   */
  public async deleteLogs(): Promise<any> {
    const del = await this.logRepo.destroy({where: {}});
    return {message: 'Логи успешно удалены', result: del};
  }

  public async restartService(servName: string): Promise<any> {
    if (servName == 'instagram') {
      return await new Promise(async (resolve, reject) => {

        Logger.slog(`[beforeApplicationShutdown] Unload all IG services.`);
        
        await this.igAccountRepo.update({status: 1}, {where: {status: {[Op.not]: 3}}});
        await this.proxyRepo.update({status: 1}, {where: {status: {[Op.not]: 3}}});

        process.on("exit", function () {
          childProcess.spawn(process.argv.shift(), process.argv, {
            cwd: process.cwd(),
            detached : true,
            stdio: "inherit"
          });
          resolve(true);
        });
        process.exit();
      });
    } else {
      return null;
    }
  }

}
