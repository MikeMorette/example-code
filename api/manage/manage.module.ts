import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { SequelizeModule } from '@nestjs/sequelize';

import { User } from '../../models/users.model';
import { Proxy } from '../../models/proxies.model';
import { Log } from '../../models/logs.model';
import { IgAccount } from '../../models/ig-accounts.model';
import { ApiLogs } from '../../models/api-logs.model';

import { UsersModule } from '../users/users.module';
import { ManageController } from './manage.controller';
import { ManageService } from './manage.service';
import { Settings } from 'src/models/settings.model';
import { Categories } from 'src/models/categories.model';
import { IgServices } from 'src/models/ig-services.model';
import { ManageGateway } from './manage.gateway';

@Module({
  imports: [
    SequelizeModule.forFeature([User, Proxy, Log, IgAccount, ApiLogs, Settings, Categories, IgServices]),
    HttpModule,
    UsersModule
  ],
  providers: [ManageGateway, ManageService],
  controllers: [ManageController],
  exports: [ManageService, ManageModule]
})
export class ManageModule {
  constructor() {}
}
